<?php
namespace app\components;

use yii\base\Component;
use yii\helpers\Html;
use yii;
use kartik\date\DatePicker;

class GridComponent extends Component
{
    public $content;
    private $headers;
    private $data;
    private $page = 5;
    private $controller;
    private $action;
    private $total_data;
    private $search_result=[];
    //private enum sort {case ASE,case DESC};

    public function init($config = [])
    {
        parent::init();
    }

    public function config($config)
    {
        $current_filter = $this->getCurrentSort();
        $this->page = $config['page'] ? $config['page'] : 5;
        $this->content = 'Hello Grid';
        $this->headers = $config['headers'];
        $this->data    = json_decode($config['data']);
        $this->total_data = $this->data;

        $search_flag = false;
        foreach($this->headers as $key=>$value){
            if(array_key_exists('search',$value) && $value['search'] ){
                if(isset($_GET[$key])){        
                    $search_flag = true;                                              
                    foreach($this->data as $Dkey=>$Dvalue){                                            
                        if(str_contains($Dvalue->$key,$_GET[$key])){
                            $this->search_result[] = $Dvalue;
                        }
                    }
                }                
            }
        }
      
        if($search_flag){
            $this->data = $this->search_result;
            $this->total_data = $this->search_result;
        }
        
        if(count($current_filter) > 1){
            $column = $current_filter[0];
            if($current_filter[1] == 'up'){
                usort($this->data, function($a, $b) use(&$column) { return $b->$column < $a->$column; });
            }
            if($current_filter[1] == 'down'){
                usort($this->data, function($a, $b) use(&$column) { return $b->$column > $a->$column; });
            }
        }

        if($this->get_dateRange_startDate() && $this->get_dateRange_endDate())
        {                        
            $this->data = $this->search_in_date_range($this->get_dateRange_startDate() , $this->get_dateRange_endDate());
        }

        $this->controller = $config['controller'];
        $this->action = $config['action'];

    }

    protected function search_in_date_range($start_date , $end_date){
        
        $date_range_data = [];
        $paymentDate = date('Y-m-d');
        foreach($this->data as $key=>$value){                                                
            $registered=date('Y-m-d', strtotime($value->registered));      
            if (($registered >= $start_date) && ($registered <= $end_date)){
                $date_range_data [] = $value;
            }
        }
        $this->total_data = $date_range_data;         
        return $date_range_data;
    }

    protected function getCurrentSort()
    {
        return explode('-',Yii::$app->getRequest()->getQueryParam('sort'));
    }

    protected function getCurrentPage(){
        return Yii::$app->getRequest()->getQueryParam('page');
    }

    protected function get_dateRange_startDate()
    {
        return Yii::$app->getRequest()->getQueryParam('startdate');
    }

    protected function get_dateRange_endDate()
    {
        return Yii::$app->getRequest()->getQueryParam('enddate');
    }

    protected function getSearchFilter()
    {
        $query_string =  $_GET;
        unset($query_string['sort']);
        unset($query_string['page']);
        return $query_string;
        
    }

    public function setPage()
    {
        $current_page = $this->getCurrentPage();
        $pgae_size= $this->page;
        $offset = 0;
        if($current_page >  1){
            $offset = ($current_page * $pgae_size) - $pgae_size;
        }

        $this->data = array_slice($this->data,$offset,$this->page);
    }

    public function render()
    {    
        $this->setPage();
        return sprintf('<table class="table">
            <thead>
            <tr>                
                %s
            </tr>
            </thead>
            <tbody>            
                %s           
            </tbody>
        </table>',$this->get_html_header(),$this->get_html_body()) . $this->setPagination();

    }

    protected function get_html_header()
    {
        return $this->render_headers();
    }

    protected function get_html_body()
    {
        return $this->render_body();
    }

    protected function render_search($key,$value)
    {
        
        $currentPage = $this->getCurrentPage();
        $currentSort = implode('-',$this->getCurrentSort());
        $page_input = '';
        $sort_input = ''; 

        if($currentPage){
            $page_input = '<input type="hidden" id="page" name="page" value="'.$currentPage.'">';
        }

        if($currentSort){
            $sort_input = '<input type="hidden" id="sort" name="sort" value="'.$currentSort.'">';
        }

        return ((array_key_exists('search',$value) && $value['search'] )  ?
         '<form method="GET">
            <div class="form-group">
            <input type="text" id="'.$key.'" name="'.$key.'" value="'.Yii::$app->getRequest()->getQueryParam($key).'" >'.$page_input.$sort_input.'
            </div>
         </form>': '');
    }

    
    public function render_headers()
    {
    
        $search_filter = http_build_query($this->getSearchFilter());
        
        $sort_order = '';
        if(count($this->getCurrentSort()) > 1)
            $sort_order = ($this->getCurrentSort()[1] == 'up'? 'down' : 'up');

        $page = 1;
        if($this->getCurrentPage()){
            $page = $this->getCurrentPage();
        } 
        
        $header_string = '';

        foreach ($this->headers as $key=> $value){
            $header_string .= '<th scope="col">
            <a href="/'.$this->controller.'/'.$this->action.'?sort='.$key.'-'.$sort_order.'&page='.$page.'&'.$search_filter.'">'
            .((array_key_exists('sort',$value))  ? '<div>||</div>': '').'</a> '.($value['title'] ? $value['title'] : $key )
            .( array_key_exists('type',$value) && $value['type'] == 'date' ? $this->get_header_date_range():'' )
            .$this->render_search($key,$value).'</th>';
        }

        $this->register_js();
        return $header_string;

    }

    protected function register_js()
    {
        $js =
        "
        $(document).ready(function(){
            $('#range-2').change(function(){
                
                start1 = $('#range').val();
                end1 = $('#range-2').val();

                if ('URLSearchParams' in window) {
                    var searchParams = new URLSearchParams(window.location.search)
                    searchParams.set('startdate', start1);
                    searchParams.set('enddate', end1);
                    var newRelativePathQuery = window.location.pathname + '?' + searchParams.toString();
                    history.pushState(null, '', newRelativePathQuery);
                }

                window.location.href = window.location.href;
                
             });
         });

        ";
        Yii::$app->getView()->registerJs($js, \yii\web\View::POS_READY);
    }

    protected function get_header_date_range()
    {   
        
        return DatePicker::widget([
            'id'=>'range',
            'name' => 'check_issue_date', 
            'name2' => 'check_issue_date',
            'value'=> $this->get_dateRange_startDate(),
            'value2'=> $this->get_dateRange_endDate(), 
            //'value' => date('d-M-Y', strtotime('+2 days')),
            'options2' => ['placeholder' => 'end date','style'=>'height:30px'],
            'options' => ['placeholder' => 'start date','style'=>'height:30px'],
            'type' => DatePicker::TYPE_RANGE,
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]);
    }

    public function render_body(){

        $body_string = '';
        foreach ($this->data as $key=>$value){
            $body_string .= '<tr>';
            foreach ($this->headers as $hkey=>$hvalue){                
                $body_string = $this->get_filed_type($body_string,$hvalue,$hkey,$value);             
            }
            $body_string .= '<tr>';
        }
        return $body_string;
    }

    protected function get_filed_type($body_string,$hvalue,$hkey,$value){
        if($hvalue['type']=='bool')
        {
            $body_string .= '<td>'.($value->$hkey ? 'فعال' : 'غیر فعال').'</td>';

        }else if($hvalue['type']=='date'){
                    
            $new_date = strtotime( $value->$hkey); 
            $body_string .= '<td>'.date('Y-m-d',$new_date).'</td>';

        }else{                                 
            $body_string .= '<td>'.$value->$hkey.'</td>';
        }   

        return $body_string;
    }

    public function setPagination()
    {
        $current_active_page = $this->getCurrentPage();        

        $prev = $_GET;
        $next = $_GET;

        if(isset($_GET['page'])){
            
            $prev['page'] = $_GET['page'] - 1;            
            $next['page'] = $_GET['page'] + 1;
        }
        

        $current = $_GET;

        $total_count = count($this->total_data);
        $page_size = (ceil($total_count / $this->page));
        $page_html = '<ul class="pagination"><li class="page-item"><a class="page-link" href="/'.$this->controller.'/'.$this->action.'?'. http_build_query($prev) .'">Previous</a></li>';

        for ($i=1;$i<$page_size+1;$i++) {
            $current['page'] = $i;
            $active = ($i == $current_active_page ? 'active' : '');
            $page_html .= '<li class="page-item '.$active.' "><a class="page-link" href="/'.$this->controller.'/'.$this->action.'?'.http_build_query($current).'">'.$i.'</a></li>';
        }

        $page_html .='<li class="page-item"><a class="page-link" href="/'.$this->controller.'/'.$this->action.'?'. http_build_query($next) .'">Next</a></li></ul>';
        return $page_html;

    }
}

?>