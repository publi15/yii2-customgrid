<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-id">

    <div class="jumbotron text-center bg-transparent">


        <?php
        //پیاده سازی
        $datasource = file_get_contents(realpath(dirname(__FILE__).'/../../views/site/test.json'));

        Yii::$app->grid->config([
                'headers'=> [
                  'id'=>['title'=>'شناسه','sort'=>true,'type'=>'number'],
                  'title'=>['title'=>'عنوان','sort'=>true,'type'=>'text','search'=>true],
                  'content'=>['title'=>'محتوا','search'=>true,'type'=>'text'],
                  'registered'=>['title'=>'تاریخ','sort'=>true,'type'=>'date'],
                  'author'=>['title'=>'نویسنده','type'=>'text'],
                  'visit_count'=>['title'=>'تعداد بازدید','type'=>'number'],
                  'isActive'=>['title'=>'وضعیت','type'=>'bool']
                ],
                'data'=> $datasource,
                'page'=>'10',
                'controller'=>'site',
                'action'=>'index'
        ]);
        echo Yii::$app->grid->render();
        ?>
    </div>
</div>